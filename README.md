# Python Dasturlash Tiliga Sayohat


## Kursda o'tiladigan mavzular
	Birinchi qism (Basic things)

	1-dars: Syntax (print, formatlash ...)

	2-dars: Variable, Data types va Operation (string, int, float, boolean, ...)

	3-dars: If else

	4-dars:Loops (for, while)

	5-dars. List

	5-dars. Tuple, Sets

	6-dars. Dictionary

	7-dars: Takrorlash

	8-dars: Functions (built-in functions, user-defined functions)

	9-dars: Kichik project qilish

	10-dars. Lambda functions

	11-dars. File operations

	Ikkinchi qism (Advanced Things)

	OOP (Object-oriented programming)

		12-dars Object, Class

		13-dars: Inheritance

		14-dars: Encapsulation

		15-dars: Polymorphism

		16-dars: Data abstraction

	17-dars: Project qilish

	18-dars: Iterators

	19-dars: Generators

	20-dars: Context Managers

	21-dars: Multithreading and Multiprocessing

	22-dars: Debugging

	23-dars: requests library

	24-dars: json bilan ishlash

	25-dars: Project qilish (har bir bola project qilib topshiradi)

	PLUS
		1. git bilan ishlash
		2. Gitlab bilan ishlash
		3. Problem solving (https://www.hackerrank.com/)


# Python Frameworks DJANGO

## Django BASIC THINGS

	1-dars: Django o'rnatish va tanishtirish (Real Project Arhitektura yasash)

	2-dars: Django models va admin ni bilan ishlash

	3-dars: Django urls va views lar bilan ishlash

	Django template language(10 dars) 

## Django REST FRAMEWORK:
	
	14-dars: REST FRAMEWORK va POSTMAN o'rganish (kirish qismi)

	15-dars: Serialization

	16-dars: Praktika qilish

	17-dars: Requests and Responses

	18-dars: Praktika qilish

	19-dars: Class-based Views

	20-dars: Praktika qilish

	21-dars: Authentication & Permissions

	22-dars: Praktika qilish

	23-dars: ViewSets & Routers

	24-dars: Praktika qilish

	25-dars: Filtering

	26-dars: Praktika qilish

	27-dars: Pagination

	28-dars: Praktika qilish

	29-dars: Umumiy takrorlash


## SQL (3 dars ko'rib chiqamiz)


## Relational databases(PostgreSQL) (2 dars)


## DJANGO ORM BILAN (object-relational mapping)
	
	1-dars. Aggregate va Annotate

	2-dars. ORDERING, VALUE, GROUP BY

	3-dars. Aggregate Functions (SUM, MIX, MAX)

	4-dars. Subquery, OuterRef

	5-dars. select_related, prefetch_related 


## Development DEVOPS (serverga yuklash)
	
	1-dars: Kirish qismi va gunicorn

	2-dars: nginx, supervisor

	3-dars: Project ni deploy qilish (Public uchun)

	4-dars: DOCKER, DOCKER-COMPUSE (dockerfile va docker-compuser file lar bilan ishlash)

	5-dars: Docker bilan project serverga quyish

	6-dars: Continuous Integration (gitlab CI\CD)


## PortFoliya uchun real project qilish (3 ta dars)
	
	Har bir o'quvchi o'zi server sotib oladi va projectni o'zi deploy qiladi.

	Ishga kirishi uchun portfoliya uchun


## TELEGRAM BOT (4 ta dars)

# Jami 72 ta dars taxminan 6 oy mo'ljallangan









